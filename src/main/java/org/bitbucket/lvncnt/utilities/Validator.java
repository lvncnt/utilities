/*
 * Copyright (c) 2018 Feng Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.lvncnt.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    /**
     * validate MM/dd/YYYY
     * @param date
     * @return
     */
    public static boolean validateDateMMDDYYYY(final String date){
        if(date == null || date.isEmpty()) return false;
        String MMDDYYYY_PATTERN = "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$";
        String MMDDYYYY_INVALID_PATTERN = "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/00[0-9]{2}$";
        Pattern pattern = Pattern.compile(MMDDYYYY_INVALID_PATTERN);
        Matcher matcher = pattern.matcher(date);
        if(matcher.matches()){
            return false;
        }
        pattern = Pattern.compile(MMDDYYYY_PATTERN);
        matcher = pattern.matcher(date);
        return matcher.matches();
    }

    private static final String PASSWORD_PATTERN =
            "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{6,20}";

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * Validate password
     * @param password
     * @return
     */
    public static boolean validatePassword(final String password){
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    /**
     * Valid email address
     * @param hex
     * @return
     */
    public static boolean validateEmailAddress(final String hex) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(hex);
        return matcher.matches();
    }
}
