/*
 * Copyright (c) 2018 Feng Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.lvncnt.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.UUID;

public class Security {

    public static final String SHA256 = "SHA-256";
    public static final String MD5 = "MD5";
    public static final String LICENSE = "https://bitbucket.org/lvncnt/licenses/raw/master/LICENSE";

    /**
     * Base64 encode
     * @param src
     * @return string in base64 encoding
     */
    public static String encode(String src) {
        if (Parse.isBlankOrNull(src)) {
            return "";
        }
        return Base64.getEncoder().withoutPadding().encodeToString(src.getBytes());
    }

    /**
     * Base 64 decode
     * @param dest
     * @return decoded base64 string
     */
    public static String decode(String dest) {
        if (Parse.isBlankOrNull(dest)) {
            return "";
        }
        return new String(Base64.getDecoder().decode(dest.replaceAll("=", "")));
    }

    /**
     * Messsage Digest of src with algo
     * @param src
     * @param algo: SHA-256, MD5
     * @return digest of src with algo
     */
    public static String digest(String src, String algo) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(algo);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        byte[] byteData = md.digest(src.getBytes());
        return bytes2Hex(byteData);
    }

    /**
     * Convert bytes array to hex string
     * @param byteData
     * @return hex string
     */
    public static String bytes2Hex(byte[] byteData){
        StringBuilder hexString = new StringBuilder();
        for(int i = 0; i < byteData.length; i ++){
            String hex = Integer.toHexString(byteData[i] & 0xFF);
            if(hex.length() == 1){
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * Get random UUID
     * @return random UUID
     */
    public static String getRandomUUID(){
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        uuid = uuid.substring(uuid.length() - 8).toUpperCase();
        return uuid;
    }

    /**
     * Get License
     * @return license string
     */
    public static String getLicense(){
        return getUrlContents(LICENSE);
    }

    private static String getUrlContents(String theUrl){
        StringBuilder content = new StringBuilder();
        try {
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch(Exception e) {
            return LICENSE;
        }
        return content.toString();
    }
}
