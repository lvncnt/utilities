/*
 * Copyright (c) 2018 Feng Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.lvncnt.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Parse {

    public static final SimpleDateFormat FORMAT_DATETIME = new java.text.SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat FORMAT_DATE_YMD = new SimpleDateFormat(
            "yyyy-MM-dd");
    public static final SimpleDateFormat FORMAT_DATE_MDY = new SimpleDateFormat(
            "MM/dd/yyyy");

    /**
     *
     * @param obj
     * @return 0 if paring failed, else parsed integer value
     */
    public static Integer tryParseInteger(Object obj) {
        Integer res;
        try {
            res = Integer.parseInt((String) obj);
        } catch (NullPointerException | NumberFormatException e) {
            res = 0;
        }
        return res;
    }

    /**
     *
     * @param obj
     * @return 0 if paring failed, else parsed float value
     */
    public static Float tryParseFloat(Object obj) {
        Float res;
        try {
            res = Float.parseFloat((String) obj);
        } catch (NullPointerException | NumberFormatException e) {
            res = 0f;
        }
        return res;
    }

    /**
     *
     * @param str in MM-DD-YYY format
     * @return parsed date if success, else current date
     */
    public static Date tryParseDateMDY(String str) {
        try {
            return FORMAT_DATE_MDY.parse(str);
        } catch (NullPointerException | ParseException e) {
            return new Date();
        }
    }

    /**
     *
     * @param str in YYYY-MM-DD format
     * @return parsed date if success, else current date
     */
    public static Date tryParseDateYMD(String str) {
        try {
            return FORMAT_DATETIME.parse(str);
        } catch (NullPointerException | ParseException e) {
            return new Date();
        }
    }

    /**
     *
     * @param str
     * @return true if str is null or empty
     */
    public static boolean isBlankOrNull(String str) {
        return (str == null || "".equals(str.trim()));
    }
}
