**Utilities-FGL**

Overall, the `Utilities-FGL` repo provides a collection of useful utilities and includes the following packages, 

* `IOUtils`, provides functions for zipping files
* `Parse`, provides functions for parsing different date format
* `Validator`, provides functions for validating date format, email address etc. 
* `Security`, provides functions for base64 encoding, decoding and random ID generator

**LICENSE**

[Apache License](https://bitbucket.org/lvncnt/licenses/raw/master/LICENSE)